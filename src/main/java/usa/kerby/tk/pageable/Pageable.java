package usa.kerby.tk.pageable;

/**
 * @author Trevor Kerby
 * @since Feb 9, 2020
 */
public interface Pageable {

	public static final String DEFAULT_LIMIT = "7";
	public static final String DEFAULT_OFFSET = "0";

	public default boolean continuesBackward() {
		return (this.getOffset() > this.getLimit()) ? true : false;
	}

	public default boolean continuesForward() {
		return ((this.getSize() - (this.getOffset() + this.getLimit())) > 0) ? true : false;
	}

	public default int nextPage() {
		return this.getOffset() + this.getLimit();
	}

	public default int firstPage() {
		return 0;
	}

	public default int previousPage() {
		return this.getOffset() - this.getLimit();
	}

	public default int lastPage() {
		return this.getSize() - this.getOffset();
	}

	public int getSize();

	public int getLimit();

	public int getOffset();

}
